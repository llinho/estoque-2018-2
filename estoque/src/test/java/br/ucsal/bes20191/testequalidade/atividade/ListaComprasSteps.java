package br.ucsal.bes20191.testequalidade.atividade;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ListaComprasSteps {
	
	private WebDriver driver;

	public ListaComprasSteps(WebDriver driver) {
		this.driver = driver;
	}
	
	@Given("estou na lista de compras")
	public void abrirListaCompra() {
		driver.get("file:///C:/Users/win/git/estoque-2018-2/estoque/src/main/webapp/lista-compras.html");
				
	}          
	

	@When("informo $quantidade para a $campoQuantidade")
	public void informarCompras(String quantidade, String campoQuantidade) {
		WebElement compras = driver.findElement(By.id(campoQuantidade));
		compras.sendKeys(quantidade);
	}



}
