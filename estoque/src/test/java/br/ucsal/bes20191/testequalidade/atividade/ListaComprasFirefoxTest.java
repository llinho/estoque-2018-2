package br.ucsal.bes20191.testequalidade.atividade;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;



public class ListaComprasFirefoxTest extends ListaComprasTestAbstract{
	
	@Override
	public InjectableStepsFactory stepsFactory() {
		System.setProperty("webdriver.gecko.driver","./drivers/geckodriver.exe");
		System.setProperty("webdriver.firefox.bin", "C:/swlami/firefox.exe");
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("marionette", true);
		WebDriver driver = (WebDriver) new FirefoxDriver(capabilities);
		return new InstanceStepsFactory(configuration(), new  ListaComprasSteps(driver));
	}

}
