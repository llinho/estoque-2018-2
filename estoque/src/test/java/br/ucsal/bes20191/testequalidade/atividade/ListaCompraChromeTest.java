package br.ucsal.bes20191.testequalidade.atividade;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.openqa.selenium.chrome.ChromeDriver;


public class ListaCompraChromeTest extends ListaComprasTestAbstract{
	
	@Override
	public InjectableStepsFactory stepsFactory() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		return new InstanceStepsFactory(configuration(), new ListaComprasSteps(driver));
	}

}
